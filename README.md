# Send notes

### EN

Plugin for Joomla 3.x that allows sending emails when notes are added to users using the system's sending data.

It also allows sending a copy to the administrator.

Send an email when a note is added to a user within the Users panel



  
  

### ES

Plugin para Joomla 3.x que permite enviar emails cuando se agreguen notas a los usuarios usando los datos de envio del sistema.

Permite además enviar una copia al administrador.

Envía un email cuando se agrega una nota a un usuario dentro del panel de Usuarios
